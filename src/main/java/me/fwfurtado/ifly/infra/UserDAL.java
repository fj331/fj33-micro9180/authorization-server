package me.fwfurtado.ifly.infra;

import me.fwfurtado.ifly.domain.User;
import me.fwfurtado.ifly.domain.UserRole;
import me.fwfurtado.ifly.domain.UserStatus;
import me.fwfurtado.ifly.login.LoginRepository;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

@Repository
public class UserDAL implements LoginRepository {
    private static final Map<String, User> DATABASE = new TreeMap<>();

    static {
        DATABASE.computeIfAbsent("fwfurtado@gmail.com", email -> new User(email, "{bcrypt}$2a$10$C3pcY4KQgtEcWNds8MZcxOSXwmE7gohTETKmYunuxASXmS8f3mZMa", UserStatus.ACTIVATED, UserRole.CUSTOMER));
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return Optional.ofNullable(DATABASE.get(email));
    }
}
