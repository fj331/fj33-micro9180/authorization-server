package me.fwfurtado.ifly.infra;

import me.fwfurtado.ifly.client.ApplicationLoginRepository;
import me.fwfurtado.ifly.domain.Application;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

@Repository
public class ApplicationDAL implements ApplicationLoginRepository {
    private static final Map<String, Application> DATABASE = new TreeMap<>();

    static {
        DATABASE.computeIfAbsent("frontend", clientId -> new Application(clientId, "{bcrypt}$2a$10$6zqeSn5FnYPdq8IZSpbmW.G4Dqc6rxQQXo3nD..ONLoBw5lS7Am9O"));
    }

    @Override
    public Optional<Application> findByClientId(String clientId) {
        return Optional.ofNullable(DATABASE.get(clientId));
    }
}
