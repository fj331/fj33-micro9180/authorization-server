package me.fwfurtado.ifly.configurations;

import me.fwfurtado.ifly.client.ClientLoginService;
import me.fwfurtado.ifly.login.LoginService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

    private final LoginService loginService;
    private final ClientLoginService clientLoginService;
    private final AuthenticationManager authenticationManager;
    private final String jwtKey;

    public AuthorizationServerConfiguration(LoginService loginService, ClientLoginService clientLoginService, AuthenticationManager authenticationManager, @Value("${jwt.key}") String jwtKey) {
        this.loginService = loginService;
        this.clientLoginService = clientLoginService;
        this.authenticationManager = authenticationManager;
        this.jwtKey = jwtKey;
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.withClientDetails(clientLoginService);
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints
                .authenticationManager(authenticationManager)
                .userDetailsService(loginService)
                .accessTokenConverter(accessTokenConverter())
                .tokenStore(tokenStore());
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security
            .checkTokenAccess("isAuthenticated()");
    }


    @Bean
    JwtTokenStore tokenStore() {
        return new JwtTokenStore(accessTokenConverter());
    }


    @Bean
    JwtAccessTokenConverter accessTokenConverter() {
        var converter = new JwtAccessTokenConverter();

        converter.setSigningKey(jwtKey);
        converter.setKeyPair();

        return converter;
    }

}
