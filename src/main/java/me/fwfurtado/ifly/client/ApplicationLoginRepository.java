package me.fwfurtado.ifly.client;

import me.fwfurtado.ifly.domain.Application;

import java.util.Optional;

public interface ApplicationLoginRepository {
    Optional<Application> findByClientId(String clientId);
}
