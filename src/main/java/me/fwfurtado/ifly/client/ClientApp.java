package me.fwfurtado.ifly.client;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class ClientApp implements ClientDetails {
    private final String clientId;
    private final String clientSecret;

    public ClientApp(String clientId, String clientSecret) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
    }

    @Override
    public String getClientId() {
        return clientId;
    }

    @Override
    public Set<String> getResourceIds() {
        return Set.of();
    }

    @Override
    public boolean isSecretRequired() {
        return true;
    }

    @Override
    public String getClientSecret() {
        return clientSecret;
    }

    @Override
    public boolean isScoped() {
        return true;
    }

    @Override
    public Set<String> getScope() {
        return Set.of("all");
    }

    @Override
    public Set<String> getAuthorizedGrantTypes() {
        return Set.of("password",  "refresh_token");
    }

    @Override
    public Set<String> getRegisteredRedirectUri() {
        return Set.of();
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return Set.of();
    }

    @Override
    public Integer getAccessTokenValiditySeconds() {
        return 60;
    }

    @Override
    public Integer getRefreshTokenValiditySeconds() {
        return 120;
    }

    @Override
    public boolean isAutoApprove(String scope) {
        return true;
    }

    @Override
    public Map<String, Object> getAdditionalInformation() {
        return Map.of();
    }
}
