package me.fwfurtado.ifly.client;

import me.fwfurtado.ifly.domain.Application;
import org.springframework.stereotype.Component;

@Component
public class ClientAppMapper {

    public ClientApp map(Application application) {
        return new ClientApp(application.getClientId(), application.getClientSecret());
    }
}
