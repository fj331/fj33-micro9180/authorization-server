package me.fwfurtado.ifly.client;

import lombok.AllArgsConstructor;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ClientLoginService implements ClientDetailsService {

    private final ApplicationLoginRepository repository;
    private final ClientAppMapper mapper;

    @Override
    public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
        return repository.findByClientId(clientId).map(mapper::map).orElseThrow(() -> new ClientRegistrationException("Invalid client"));
    }
}
