package me.fwfurtado.ifly.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Application {
    private String clientId;
    private String clientSecret;
}
