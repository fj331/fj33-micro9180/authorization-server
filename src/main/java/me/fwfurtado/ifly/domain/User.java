package me.fwfurtado.ifly.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import static java.util.Arrays.stream;


@Getter
@EqualsAndHashCode
public class User {
    private String email;
    private String password;
    private UserStatus status;
    private String[] roles;

    public User(String email, String password, UserStatus status, UserRole... roles) {
        this.email = email;
        this.password = password;
        this.status = status;
        this.roles = stream(roles).map(UserRole::name).toArray(String[]::new);
    }
}
