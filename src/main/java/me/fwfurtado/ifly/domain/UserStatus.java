package me.fwfurtado.ifly.domain;

public enum UserStatus {
    ACTIVATED, INACTIVATED;
}
