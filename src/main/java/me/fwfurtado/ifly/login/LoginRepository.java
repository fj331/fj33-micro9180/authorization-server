package me.fwfurtado.ifly.login;

import me.fwfurtado.ifly.domain.User;

import java.util.Optional;

public interface LoginRepository {
    Optional<User> findByEmail(String email);
}
