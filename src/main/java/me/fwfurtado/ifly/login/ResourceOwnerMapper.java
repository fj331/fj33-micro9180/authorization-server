package me.fwfurtado.ifly.login;

import me.fwfurtado.ifly.domain.User;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Component;

@Component
public class ResourceOwnerMapper {
    public ResourceOwner map(User user) {

        var authorities = AuthorityUtils.createAuthorityList(user.getRoles());

        return new ResourceOwner(user.getEmail(), user.getPassword(), authorities, user.getStatus() );
    }
}
